/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.List;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.MPSubgroup;
import nl.krocket.push.schema.SearchRequest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sander
 */
public class SearchRequestDAOTest {
    
    public SearchRequestDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
        @Before
    public void setUp() {
        DaoTestHelper.getMobileDevice(deviceId);
    }
    
    @After
    public void tearDown() {
    }

    private final String deviceId="e49476c5-2513-434c-9800-9d010fed5093";

    @Test
    public void testSaveCarSearchRequest() {
        SearchRequest searchRequest = DaoTestHelper.getCarSearchRequest(deviceId);
        SearchRequestDAO instance = new SearchRequestDAO();
        instance.saveSearchRequest(searchRequest);
    }
    
    @Test
    public void testSaveClothesSearchRequest() {
        SearchRequest searchRequest = DaoTestHelper.getClothesSearchRequest(deviceId);
        SearchRequestDAO instance = new SearchRequestDAO();
        instance.saveSearchRequest(searchRequest);
    }

    @Test
    public void testGetCarSearchRequest() {
        SearchRequest searchRequest = DaoTestHelper.getCarSearchRequest(deviceId);
        SearchRequestDAO instance = new SearchRequestDAO();
        instance.saveSearchRequest(searchRequest);
        String searchRequestId = searchRequest.getOfferId();
        instance = new SearchRequestDAO();
        SearchRequest expResult = searchRequest;
        SearchRequest result = instance.getCarSearchRequest(searchRequestId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetClothesSearchRequest() {
        SearchRequest searchRequest = DaoTestHelper.getClothesSearchRequest(deviceId);
        SearchRequestDAO instance = new SearchRequestDAO();
        instance.saveSearchRequest(searchRequest);
        String searchRequestId = searchRequest.getOfferId();
        instance = new SearchRequestDAO();
        SearchRequest expResult = searchRequest;
        SearchRequest result = instance.getClothesSearchRequest(searchRequestId);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetSearchRequestsOnCars() {
        MPGroup group = MPGroup.Autos;
        SearchRequestDAO instance = new SearchRequestDAO();
        List<SearchRequest> result = instance.getSearchRequests(group);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testGetSearchRequestsOnJurken() {
        MPSubgroup supgroup = MPSubgroup.Jurken;
        SearchRequestDAO instance = new SearchRequestDAO();
        List<SearchRequest> result = instance.getSearchRequests(supgroup);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testGetSearchRequestsOnDevice() {
        SearchRequestDAO instance = new SearchRequestDAO();
        List<SearchRequest> result = instance.getSearchRequests(this.deviceId);
        assertTrue(result.size()>0);
    }

    @Test
    public void testDeleteCarSearchRequest() {
        SearchRequest searchRequest = DaoTestHelper.getCarSearchRequest(deviceId);
        SearchRequestDAO instance = new SearchRequestDAO();
        instance.saveSearchRequest(searchRequest);
        String searchRequestId = searchRequest.getOfferId();
        instance = new SearchRequestDAO();
        instance.deleteCarSearchRequest(searchRequestId);
        instance = new SearchRequestDAO();
        SearchRequest result = instance.getCarSearchRequest(searchRequestId);
        assertNull(result);
    }
    
    @Test
    public void testDeleteClothesSearchRequest() {
        SearchRequest searchRequest = DaoTestHelper.getClothesSearchRequest(deviceId);
        SearchRequestDAO instance = new SearchRequestDAO();
        instance.saveSearchRequest(searchRequest);
        String searchRequestId = searchRequest.getOfferId();
        instance = new SearchRequestDAO();
        instance.deleteClothesSearchRequest(searchRequestId);
        instance = new SearchRequestDAO();
        SearchRequest result = instance.getCarSearchRequest(searchRequestId);
        assertNull(result);
    }
    
}
