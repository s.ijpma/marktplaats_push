/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.ArrayList;
import java.util.List;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesSearchRequest;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.schema.MPSubgroup;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sander
 */
public class OfferDAOTest {
    
    public OfferDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DaoTestHelper.getMobileDevice(deviceId);
    }
    
    @After
    public void tearDown() {
    }

    private final String deviceId="e49476c5-2513-434c-9800-9d010fed5093";

    @Test
    public void testSaveCarOffer() {
        Offer offer = DaoTestHelper.getCarOffer(deviceId);
        OfferDAO instance = new OfferDAO();
        instance.saveOffer(offer);
    }
    
    @Test
    public void testSaveClothesOffer() {
        Offer offer = DaoTestHelper.getClothesOffer(deviceId);
        OfferDAO instance = new OfferDAO();
        instance.saveOffer(offer);
    }

    @Test
    public void testGetCarOffer() {
        Offer offer = DaoTestHelper.getCarOffer(deviceId);
        OfferDAO instance = new OfferDAO();
        instance.saveOffer(offer);
        String offerId = offer.getOfferId();
        instance = new OfferDAO();
        Offer expResult = offer;
        Offer result = instance.getCarOffer(offerId);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetClothesOffer() {
        Offer offer = DaoTestHelper.getClothesOffer(deviceId);
        OfferDAO instance = new OfferDAO();
        instance.saveOffer(offer);
        String offerId = offer.getOfferId();
        instance = new OfferDAO();
        Offer expResult = offer;
        Offer result = instance.getClothesOffer(offerId);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOffersOnCars() {
        MPGroup group = MPGroup.Autos;
        OfferDAO instance = new OfferDAO();
        List<Offer> result = instance.getOffers(group);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testGetOffersOnDevice() {
        OfferDAO instance = new OfferDAO();
        List<Offer> result = instance.getOffers(this.deviceId);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testGetOffersOnDeviceOnlyOffers() {
        OfferDAO service = new OfferDAO();
        List<Offer> offerList = service.getOffers(deviceId);
        List<Offer> newList = new ArrayList<>();
        for (Offer offer : offerList) {
            if (offer instanceof CarSearchRequest) {
                
            }
            else if (offer instanceof ClothesSearchRequest) {
                
            }
            else {
                newList.add(offer);
            }
        }
        assertTrue(newList.size()>0);
    }
    
    @Test
    public void testGetOffersOnJurken() {
        MPSubgroup supgroup = MPSubgroup.Jurken;
        OfferDAO instance = new OfferDAO();
        List<Offer> result = instance.getOffers(supgroup);
        assertTrue(result.size()>0);
    }

    @Test
    public void testDeleteCarOffer() {
        Offer offer = DaoTestHelper.getCarOffer(deviceId);
        OfferDAO instance = new OfferDAO();
        instance.saveOffer(offer);
        String offerId = offer.getOfferId();
        instance = new OfferDAO();
        instance.deleteCarOffer(offerId);
        instance = new OfferDAO();
        Offer result = instance.getCarOffer(offerId);
        assertNull(result);
    }
    
    @Test
    public void testDeleteClothesOffer() {
        Offer offer = DaoTestHelper.getClothesOffer(deviceId);
        OfferDAO instance = new OfferDAO();
        instance.saveOffer(offer);
        String offerId = offer.getOfferId();
        instance = new OfferDAO();
        instance.deleteClothesOffer(offerId);
        instance = new OfferDAO();
        Offer result = instance.getCarOffer(offerId);
        assertNull(result);
    }
    
}
