/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 18, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.List;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.schema.SearchRequest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sander
 */
public class MatchDAOTest {
    
    public MatchDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private final String deviceId="e49476c5-2513-434c-9800-9d010fed5093";
    
    @Test
    public void testPerformMatch_CarOffer() {
        Offer offer = DaoTestHelper.getCarOffer(deviceId);
        MatchDAO instance = new MatchDAO();
        List<SearchRequest> result = instance.performMatch(offer);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testPerformMatch_ClothesOffer() {
        Offer offer = DaoTestHelper.getClothesOffer(deviceId);
        MatchDAO instance = new MatchDAO();
        List<SearchRequest> result = instance.performMatch(offer);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testPerformMatch_CarSearchRequest() {
        SearchRequest request = DaoTestHelper.getCarSearchRequest(deviceId);
        MatchDAO instance = new MatchDAO();

        List<Offer> result = instance.performMatch(request);
        assertTrue(result.size()>0);
    }
    
    @Test
    public void testPerformMatch_ClothesSearchRequest() {
        SearchRequest request = DaoTestHelper.getClothesSearchRequest(deviceId);
        MatchDAO instance = new MatchDAO();

        List<Offer> result = instance.performMatch(request);
        assertTrue(result.size()>0);
    }
    
}
