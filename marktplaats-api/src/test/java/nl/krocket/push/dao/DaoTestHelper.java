/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.io.IOException;
import java.util.Date;
import nl.krocket.push.schema.CarOffer;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesOffer;
import nl.krocket.push.schema.ClothesSearchRequest;
import nl.krocket.push.schema.ClothesSize;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.MPSubgroup;
import nl.krocket.push.schema.MobileDevice;
import nl.krocket.push.schema.MobileDeviceType;
import nl.krocket.push.schema.OfferImage;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class DaoTestHelper {
    
    public static String toJson(Object o) {
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try {
            result = mapper.writeValueAsString(o);
            
        } catch (IOException ex) {
            
        }
        return result;
    }
    
    public static CarOffer getCarOffer(String deviceId) {
        CarOffer offer = new CarOffer();
        offer.setDateOfApk(new Date());
        offer.setDateOfEntry(new Date());
        offer.setPrice(new Double("3000"));
        offer.setSubGroup(MPSubgroup.Audi);
        offer.setType("A6");
        offer.setDescription("Nette snelle auto!");
        offer.setDevive(getMobileDevice(deviceId));
        offer.getImages().add(getOfferImage("a6.jpg"));
        System.out.println("CarOffer" + toJson(offer));
        return offer;
    }
    
    public static OfferImage getOfferImage(String name) {
        OfferImage image = new OfferImage();
        image.setName(name);
        image.setUrl("http://localhost:8080/marktplaats-api/uploads/" + name);
        return image;
    }
    
    public static ClothesOffer getClothesOffer(String deviceId) { 
        ClothesOffer offer = new ClothesOffer();
        offer.setPrice(new Double("12.75"));
        offer.setMainGroup(MPGroup.Kleding_dames);
        offer.setSubGroup(MPSubgroup.Jurken);
        offer.setDevice(getMobileDevice(deviceId));
        offer.getImages().add(getOfferImage("jurk.jpg"));
        offer.setDescription("Mooie roze jurk");
        offer.setColor("Red");
        offer.setClothesSize(ClothesSize.M);
        System.out.println("ClothesOffer: " + toJson(offer));
        return offer;
    }
    
    public static MobileDevice getMobileDevice(String deviceId) {
        MobileDevice device = new MobileDevice();
        device.setDeviceId(deviceId);
        device.setType(MobileDeviceType.Android);
        device.setAlias("Alias");
        System.out.println("MobileDevice: " + toJson(device));
        return device;
    }
    
    public static MobileDevice saveOrGetMobileDevice(String deviceId) {
        DeviceDAO instance = new DeviceDAO();
        MobileDevice device = instance.getDevice(deviceId);
        if (device==null) {
            instance = new DeviceDAO();
            device = DaoTestHelper.getMobileDevice(deviceId);
            instance.saveDevice(device);
        }
        return device;
    }

    public static CarSearchRequest getCarSearchRequest(String deviceId) {
        CarSearchRequest request = new CarSearchRequest();
        request.setDateOfApk(new Date());
        request.setDateOfEntry(new Date());
        request.setPriceFrom(new Double("2000"));
        request.setPriceTo(new Double("5000"));
        request.setSubGroup(MPSubgroup.Audi);
        request.setType("A6");
        request.setDevive(getMobileDevice(deviceId));
        System.out.println("CarSearchRequest: " + toJson(request));
        return request;
    }

    static ClothesSearchRequest getClothesSearchRequest(String deviceId) {
        ClothesSearchRequest request = new ClothesSearchRequest();
        request.setPriceFrom(new Double("10"));
        request.setPriceTo(new Double("15"));
        request.setMainGroup(MPGroup.Kleding_dames);
        request.setSubGroup(MPSubgroup.Jurken);
        request.setDevice(getMobileDevice(deviceId));
        request.setClothesSize(ClothesSize.M);
        System.out.println("ClothesSearchRequest: " + toJson(request));
        return request;
    }

}
