/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.List;
import nl.krocket.push.schema.MobileDevice;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sander
 */
public class DeviceDAOTest {
    
    public DeviceDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private final String deviceId="4b30bf91-e094-4a40-b161-b18f61462c1d";
    
    @Test
    public void testSaveDevice() {
        MobileDevice device = DaoTestHelper.getMobileDevice(deviceId);
        DeviceDAO instance = new DeviceDAO();
        instance.saveDevice(device);
        instance = new DeviceDAO();
        device.setDeviceId("e49476c5-2513-434c-9800-9d010fed5093");
        instance.saveDevice(device);
    }
    
    @Test
    public void testGetDevice() {
        DeviceDAO instance = new DeviceDAO();
        MobileDevice device = instance.getDevice(deviceId);
        assertEquals(device.getDeviceId(), deviceId);
    }
    
    @Test
    public void testGetDevices() {
        DeviceDAO instance = new DeviceDAO();
        List<MobileDevice> result = instance.getDevices();
        assertTrue(result.size()>=1);
    }

    @Test
    public void testDeleteDevice() {
        DeviceDAO instance = new DeviceDAO();
        List<MobileDevice> result = instance.getDevices();
        int sizeBefore = result.size();
        instance = new DeviceDAO();
        instance.deleteDevice(deviceId);
        instance = new DeviceDAO();
        result = instance.getDevices();
        int sizeAfter = result.size();
        assertEquals(sizeAfter, sizeBefore-1);
    }

}
