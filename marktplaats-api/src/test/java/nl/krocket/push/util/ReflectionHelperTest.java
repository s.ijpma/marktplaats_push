/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 18, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.util;

import nl.krocket.push.schema.CarOffer;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesOffer;
import nl.krocket.push.schema.ClothesSearchRequest;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sander
 */
public class ReflectionHelperTest {
    
    public ReflectionHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCarSearchRequestObject() throws Exception {
        String className = "nl.krocket.push.schema.CarSearchRequest";
        Object o = ReflectionHelper.getObject(className);
        ObjectMapper mapper = new ObjectMapper();
        String param = mapper.writeValueAsString(o);
        assertTrue(o instanceof CarSearchRequest);
        System.out.println(param);
    }
    
    @Test
    public void testGetClothesSearchRequestObject() throws Exception {
        String className = "nl.krocket.push.schema.ClothesSearchRequest";
        Object o = ReflectionHelper.getObject(className);
        ObjectMapper mapper = new ObjectMapper();
        String param = mapper.writeValueAsString(o);
        assertTrue(o instanceof ClothesSearchRequest);
        System.out.println(param);
    }
    
    @Test
    public void testGetCarOfferObject() throws Exception {
        String className = "nl.krocket.push.schema.CarOffer";
        Object o = ReflectionHelper.getObject(className);
        ObjectMapper mapper = new ObjectMapper();
        String param = mapper.writeValueAsString(o);
        assertTrue(o instanceof CarOffer);
        System.out.println(param);
    }
    
    @Test
    public void testGetClothesOfferObject() throws Exception {
        String className = "nl.krocket.push.schema.ClothesOffer";
        Object o = ReflectionHelper.getObject(className);
        ObjectMapper mapper = new ObjectMapper();
        String param = mapper.writeValueAsString(o);
        assertTrue(o instanceof ClothesOffer);
        System.out.println(param);
    }

}
