/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.services;

import java.util.ArrayList;
import java.util.List;
import nl.krocket.push.dao.DaoTestHelper;
import nl.krocket.push.schema.Offer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sander
 */
public class UAPushServiceTest {
    
    public UAPushServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSendAndroidPush() {
        List<String> searchRequestApids = new ArrayList<>();
        searchRequestApids.add("6af95977-9c3f-4594-9c01-cbbe639183ba");
        Offer offer = DaoTestHelper.getCarOffer("e49476c5-2513-434c-9800-9d010fed5093");
        UAPushService instance = new UAPushService();
        instance.sendAndroidPush(searchRequestApids, offer);
    }
       
}
