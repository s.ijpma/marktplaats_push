/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 26, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.services;

import nl.krocket.push.dao.DaoTestHelper;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.schema.SearchRequest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sander
 */
public class MatchResultServiceTest {
    
    public MatchResultServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private final String apid="6af95977-9c3f-4594-9c01-cbbe639183ba";
    
    @Test
    public void testDoMatchingAndPush_Offer() {
        Offer offer = DaoTestHelper.getCarOffer(apid);
        MatchResultService instance = new MatchResultService();
        instance.doMatchingAndPush(offer);
    }
    
    @Test
    public void testDoMatchingAndPush_SearchRequest() {
        SearchRequest request = DaoTestHelper.getCarSearchRequest(apid);
        MatchResultService instance = new MatchResultService();
        instance.doMatchingAndPush(request);
    }
    
}
