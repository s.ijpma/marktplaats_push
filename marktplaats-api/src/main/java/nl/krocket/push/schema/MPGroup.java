/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public enum MPGroup {
    
    Autos,
    Kleding_dames,
    Kleding_heren;
    
    public static List<MPGroup> getAsList() {
        List<MPGroup> retValues = new ArrayList<>();
        MPGroup[] values = MPGroup.values();
        retValues.addAll(Arrays.asList(values));
        return retValues;
    }
}
