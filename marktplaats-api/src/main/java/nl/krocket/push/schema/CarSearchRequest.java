/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Entity
@Table( name = "carsearch" )
public class CarSearchRequest extends CarOffer implements SearchRequest  {

    private Double priceFrom;
    private Double priceTo;
    
    public CarSearchRequest() {
        this.setPrice(new Double(0));
    }
    
    @Transient
    @Override
    public Set<OfferImage> getImages() {
        return null;
    }

    @Override
    public Double getPriceFrom() {
        return priceFrom;
    }

    @Override
    public Double getPriceTo() {
        return priceTo;
    }

    @Override
    public void setPriceFrom(Double priceFrom) {
        this.priceFrom = priceFrom;
    }

    @Override
    public void setPriceTo(Double priceTo) {
        this.priceTo = priceTo;
    }



}
