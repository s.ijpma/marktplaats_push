/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 *//*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Transient;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public interface Offer extends Serializable {
    
    @Transient
    public String getOfferId();
    
    @Transient
    public MobileDevice getDevice();
    
    @Transient
    public Date getCreationDate();

    @Transient
    public MPGroup getMainGroup();
    
    @Transient
    public MPSubgroup getSubGroup();

    @Transient
    public Double getPrice();
    
    @Transient
    public String getDescription();
    
    @Transient
    public Set<OfferImage> getImages();
    
    @Transient
    public Set<MobileDevice> getSentTo();
    
}
