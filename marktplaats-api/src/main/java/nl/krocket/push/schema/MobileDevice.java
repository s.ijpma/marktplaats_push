/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Entity
@Table( name = "mobiledevice" )
public class MobileDevice implements Serializable {
    
    @Id
    @NotNull(message="deviceId should be filled")
    private String deviceId;
    @Enumerated(EnumType.STRING)
    @NotNull(message="deviceType should be filled")
    private MobileDeviceType type;
    private String alias;
    
    public MobileDevice() {
        
    }

    public MobileDeviceType getType() {
        return type;
    }

    public void setType(MobileDeviceType type) {
        this.type = type;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.deviceId);
        hash = 37 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MobileDevice other = (MobileDevice) obj;
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.alias, other.alias)) {
            return false;
        }
        return true;
    }
    
    

}
