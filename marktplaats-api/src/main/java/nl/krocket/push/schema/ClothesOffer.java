/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Entity
@Table( name = "clothesoffer" )
public class ClothesOffer implements Offer {
    
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String offerId;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.DETACH)
    @JoinColumn(name = "deviceId", nullable = false)
    private MobileDevice device;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private final Date creationDate;
    
    @Enumerated(EnumType.STRING)
    private MPGroup mainGroup;
    
    @Enumerated(EnumType.STRING)
    private MPSubgroup subGroup;
    
    private Double price;
    
    @JsonDeserialize(as = HashSet.class, contentAs = OfferImage.class)
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private Set<OfferImage> images;
    
    @JsonDeserialize(as = HashSet.class, contentAs = MobileDevice.class)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch=FetchType.EAGER)
    private Set<MobileDevice> sentTo;
    
    private String description;
    
    @Enumerated(EnumType.STRING)
    private ClothesSize clothesSize;
    
    private String color;

    public ClothesOffer() {
        this.creationDate = new Date();
    }
    
    @Override
    public String getOfferId() {
        return offerId;
    }

    @Override
    public MPGroup getMainGroup() {
        return mainGroup;
    }
    
    public void setMainGroup(MPGroup mainGroup) {
        this.mainGroup = mainGroup;
    }

    @Override
    public MPSubgroup getSubGroup() {
        return subGroup;
    }

    @Override
    public MobileDevice getDevice() {
        return device;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Set<OfferImage> getImages() {
        if (this.images==null) {
            this.images = new HashSet<>();
        }
        return this.images;
    }

    public void setDevice(MobileDevice device) {
        this.device = device;
    }

    public void setGroup(MPGroup group) {
        this.mainGroup = group;
    }

    public void setSubGroup(MPSubgroup subGroup) {
        this.subGroup = subGroup;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ClothesSize getClothesSize() {
        return clothesSize;
    }

    public void setClothesSize(ClothesSize clothesSize) {
        this.clothesSize = clothesSize;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public Set<MobileDevice> getSentTo() {
        if (this.sentTo == null) {
            this.sentTo = new HashSet<>();
        }
        return this.sentTo;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.offerId);
        hash = 13 * hash + Objects.hashCode(this.device);
        hash = 13 * hash + Objects.hashCode(this.creationDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClothesOffer other = (ClothesOffer) obj;
        if (!Objects.equals(this.offerId, other.offerId)) {
            return false;
        }
        if (!Objects.equals(this.device, other.device)) {
            return false;
        }
        if (this.mainGroup != other.mainGroup) {
            return false;
        }
        if (this.subGroup != other.subGroup) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return true;
    }
    
    
}
