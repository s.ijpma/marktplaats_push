/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.util.ArrayList;
import java.util.List;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public enum MPSubgroup {
    
    Jassen(MPGroup.Kleding_dames),
    Jurken(MPGroup.Kleding_dames),
    Rokken(MPGroup.Kleding_dames),
    
    Polos(MPGroup.Kleding_heren),
    Overhemden(MPGroup.Kleding_heren),
    Stropdassen(MPGroup.Kleding_heren),
    
    Audi(MPGroup.Autos),
    BMW(MPGroup.Autos),
    Volvo(MPGroup.Autos);
    
    MPGroup mainGroup;

    private MPSubgroup(MPGroup mainGroup) {
        this.mainGroup=mainGroup;
    }
    public MPGroup getParent() {
        return this.mainGroup;
    }
    public static List<MPSubgroup> getAsList(MPGroup parent) {
        List<MPSubgroup> retValues = new ArrayList<>();
        MPSubgroup[] values = MPSubgroup.values();
        for (MPSubgroup mPSubgroup : values) {
            if (mPSubgroup.getParent().equals(parent)) {
                retValues.add(mPSubgroup);
            }
        }
        return retValues;
    }
}
