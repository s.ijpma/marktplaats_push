/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import javax.persistence.Transient;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public interface SearchRequest extends Offer {
    
    @Transient
    public Double getPriceFrom();
    
    @Transient
    public Double getPriceTo();
    
    @Transient
    public void setPriceFrom(Double priceFrom);
    
    @Transient
    public void setPriceTo(Double priceTo);

}
