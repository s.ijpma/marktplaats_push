/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public enum ClothesSize {

    XS,
    S,
    M,
    L,
    XL,
    XXL;
    
    public static List<ClothesSize> getAsList() {
        List<ClothesSize> retValues = new ArrayList<>();
        ClothesSize[] values = ClothesSize.values();
        retValues.addAll(Arrays.asList(values));
        return retValues;
    }
}
