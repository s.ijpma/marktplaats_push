/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.schema;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class PriceRange {
    
    private double priceFrom;
    private double priceTo;

    public double getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(double priceFrom) {
        this.priceFrom = priceFrom;
    }

    public double getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(double priceTo) {
        this.priceTo = priceTo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.priceFrom) ^ (Double.doubleToLongBits(this.priceFrom) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.priceTo) ^ (Double.doubleToLongBits(this.priceTo) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PriceRange other = (PriceRange) obj;
        if (Double.doubleToLongBits(this.priceFrom) != Double.doubleToLongBits(other.priceFrom)) {
            return false;
        }
        if (Double.doubleToLongBits(this.priceTo) != Double.doubleToLongBits(other.priceTo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PriceRange{" + "priceFrom=" + priceFrom + ", priceTo=" + priceTo + '}';
    }
    
    

}
