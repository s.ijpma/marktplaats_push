/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.services;

import java.util.ArrayList;
import java.util.List;
import nl.krocket.push.dao.MatchDAO;
import nl.krocket.push.dao.OfferDAO;
import nl.krocket.push.dao.SearchRequestDAO;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.schema.SearchRequest;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class MatchResultService {
    
    
    public void doMatchingAndPush(SearchRequest request) {
        MatchDAO service = new MatchDAO();
        List<Offer> offerList = service.performMatch(request);
        if (offerList!=null) {
            UAPushService pushService = new UAPushService();
            List<String> apids = new ArrayList<>();
            apids.add(request.getDevice().getDeviceId());
            for (Offer offer : offerList) {
                if (!offer.getSentTo().contains(request.getDevice())) {
                    pushService.sendAndroidPush(apids, offer);
                    offer.getSentTo().add(request.getDevice());
                    OfferDAO offerDAO = new OfferDAO();
                    offerDAO.saveOffer(offer);
                }
            }
        }
    }
    
    public void doMatchingAndPush(Offer offer) {
        MatchDAO service = new MatchDAO();
        List<SearchRequest> searchRequestList = service.performMatch(offer);
        if (searchRequestList!=null) {
            UAPushService pushService = new UAPushService();
            List<String> apids = new ArrayList<>();
            for (SearchRequest searchRequest : searchRequestList) {
                if (!searchRequest.getSentTo().contains(offer.getDevice())) {
                    apids.add(searchRequest.getDevice().getDeviceId());
                    searchRequest.getSentTo().add(searchRequest.getDevice());
                    SearchRequestDAO searchRequestDAO = new SearchRequestDAO();
                    searchRequestDAO.saveSearchRequest(searchRequest);
                }
            }
            if (apids.size()>=1) {
                pushService.sendAndroidPush(apids, offer);
            }
        }
    }


}
