/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.services;

import com.urbanairship.api.client.APIClient;
import com.urbanairship.api.client.APIClientResponse;
import com.urbanairship.api.client.APIPushResponse;
import com.urbanairship.api.client.APIRequestException;
import com.urbanairship.api.push.model.Platform;
import com.urbanairship.api.push.model.PlatformData;
import com.urbanairship.api.push.model.PushPayload;
import com.urbanairship.api.push.model.audience.Selectors;
import com.urbanairship.api.push.model.notification.Notification;
import com.urbanairship.api.push.model.notification.Notifications;
import com.urbanairship.api.push.model.notification.android.AndroidDevicePayload;
import com.urbanairship.api.push.model.notification.richpush.RichPushMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.util.HttpHeaderUtil;
import nl.krocket.push.util.RichPushMessageHelper;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class UAPushService {
    
    Logger logger = Logger.getLogger(UAPushService.class.getName());
    static String appKey = "iC2iwNG2Qq2_r_CaWN5x6Q";
    static String appSecret = "HP-B_zEGRsy1eVIoFDLvJQ";
    
    static String serverApiUrl = "https://go.urbanairship.com/";
    
    static String template = "richpushmessage.vm";
    
    public void sendAndroidPush(List<String> searchRequestApids, Offer offer) {
        final Map model = new HashMap();
        model.put("offer", offer);
        String alert="Nieuwe aanbieding in " + offer.getSubGroup().toString();
        String title=alert;
        sendAndroidPush(searchRequestApids, alert, title, model);
    }
    
    private void sendAndroidPush(List<String> apids, String alert, String title, Map model) {
        // Build and configure an APIClient
        APIClient apiClient = APIClient.newBuilder()
                .setKey(appKey)
                .setSecret(appSecret)
                .build();
        
        //setup android payload
        AndroidDevicePayload androidDevicePayload = AndroidDevicePayload.newBuilder()
                .setAlert(alert)
                .build();

        Notification notification = Notifications.notification(androidDevicePayload);
        
        String body = RichPushMessageHelper.getBody(template, model);
        RichPushMessage message = RichPushMessageHelper.getHtmlMessage(title, body);
        
        // Setup a payload for the message you want to send
        PushPayload payload = PushPayload.newBuilder()
                .setAudience(Selectors.apids(apids))
                //.setNotification(Notifications.alert("API v3"))
                .setNotification(notification)
                .setMessage(message)
                .setPlatforms(PlatformData.of(Platform.ANDROID))
                .build();
    // Try/Catch for any issues, any non 200 response, or non library
        // related exceptions
        try {
            APIClientResponse<APIPushResponse> response = apiClient.push(payload);
            logger.debug(String.format("Response %s", response.toString()));
        } catch (APIRequestException ex) {
            logger.error(String.format("APIRequestException " + ex));
            logger.error("Something wrong with the request " + ex.toString());
        } catch (IOException e) {
            logger.error("IOException in API request " + e.getMessage());
        }
    }
    
    public void sendAndroidDevice(String apids) throws RestClientException{
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = HttpHeaderUtil.getJsonHttpHeadersBasicAuth(appKey, appSecret);
        HttpEntity<Object> request = new HttpEntity<>(headers);
        String url = serverApiUrl + "api/apids/"+apids;
        restTemplate.put(url, request);
    }
    
    public void getAndroidDevices() {
        
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = HttpHeaderUtil.getJsonHttpHeadersBasicAuth(appKey, appSecret);
        HttpEntity<Object> request = new HttpEntity<>(headers);
        String url = serverApiUrl + "api/apids/";
        try {
            Object o = null;
            ResponseEntity result = restTemplate.exchange(url, HttpMethod.GET, request, String.class, o);
            System.out.println(result.toString());
        }
        catch (RestClientException rc) {
            logger.error(rc.getMessage(), rc);
        }
        
    }

}
