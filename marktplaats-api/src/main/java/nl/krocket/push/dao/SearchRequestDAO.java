/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.ArrayList;
import java.util.List;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesSearchRequest;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.MPSubgroup;
import nl.krocket.push.schema.SearchRequest;
import nl.krocket.push.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class SearchRequestDAO {
    
    private final SessionFactory sessionFactory;
    
    public SearchRequestDAO() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    
    public void saveSearchRequest(SearchRequest searchRequest) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate( searchRequest );
            session.getTransaction().commit();
            session.close();
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("SearchRequest not saved or updated", e);
        }
    }
    
    public CarSearchRequest getCarSearchRequest(String searchId) {
        return (CarSearchRequest)this.getSearchRequest(searchId, "CarSearchRequest");
    }
    
    public ClothesSearchRequest getClothesSearchRequest(String searchId) {
        return (ClothesSearchRequest)this.getSearchRequest(searchId, "ClothesSearchRequest");
    }
    
    private SearchRequest getSearchRequest(String searchId, String tableName) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from "+tableName+" as u where u.offerId = :offerId" );
            query.setString("offerId", searchId);
            List result = query.list();
            if (!result.isEmpty()) {
                SearchRequest m = (SearchRequest)result.get(0);
                session.close();
                return m;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("SearchRequest not loaded, searchId:" + searchId, e);
        }
    }
    
    public List<SearchRequest> getSearchRequests(MPGroup group) {
        Session session = sessionFactory.openSession();
        String table = group.equals(MPGroup.Autos) ? "CarSearchRequest" : "ClothesSearchRequest";
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from "+table );
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("SearchRequests not loaded for group:" + group.name(), e);
        }
    }

    public List<SearchRequest> getSearchRequests(MPSubgroup supgroup) {
        Session session = sessionFactory.openSession();
        String table = supgroup.getParent().equals(MPGroup.Autos) ? "CarSearchRequest" : "ClothesSearchRequest";
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from "+table+" as t where t.subGroup = :subGroup" );
            query.setString("subGroup", supgroup.toString());
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("SearchRequests not loaded for subgroup:" + supgroup.name(), e);
        }
    }
    
    public List<SearchRequest> getSearchRequests(String deviceId) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            List result = session.createCriteria(SearchRequest.class).add(Restrictions.eq("device.deviceId", deviceId)).list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return new ArrayList<>();
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("SearchRequests not loaded for device:" + deviceId, e);
        }
    }
    
    public void deleteCarSearchRequest(String searchId) {
        this.deleteSearchRequest(searchId, "CarSearchRequest");
    }
    
    public void deleteClothesSearchRequest(String searchId) {
        this.deleteSearchRequest(searchId, "ClothesSearchRequest");
    }
    
    private void deleteSearchRequest(String searchId, String tableName) {
        SearchRequest searchRequest = getSearchRequest(searchId, tableName);
        deleteSearchRequest(searchRequest);
    }
    
    private void deleteSearchRequest(SearchRequest searchRequest) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(searchRequest);
            session.getTransaction().commit();
            session.close();
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("SearchRequest not deleted", e);
        }
    }

}
