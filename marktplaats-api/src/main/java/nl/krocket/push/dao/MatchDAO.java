/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.List;
import nl.krocket.push.schema.CarOffer;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesOffer;
import nl.krocket.push.schema.ClothesSearchRequest;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.schema.SearchRequest;
import nl.krocket.push.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class MatchDAO {
    
    private final SessionFactory sessionFactory;
    
    public MatchDAO() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    
    public List<SearchRequest> performMatch(Offer offer) {
        if (offer instanceof CarOffer) {
            return performCarMatch((CarOffer)offer);
        }
        else {
            return performClothesMatch((ClothesOffer)offer);
        }
    }
    
    public List<Offer> performMatch(SearchRequest request) {
        if (request instanceof CarSearchRequest) {
            return performCarMatch((CarSearchRequest)request);
        }
        else {
            return performClothesMatch((ClothesSearchRequest)request);
        }
    }
    
    private List<SearchRequest> performCarMatch(CarOffer offer) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from CarSearchRequest as u where "
                    + "u.mainGroup = :mainGroup "
                    + "and u.subGroup = :subGroup "
                    + "and u.type = :type "
                    + "and u.priceFrom <= :price "
                    + "and u.priceTo >= :price" );
            
            query.setString("mainGroup", offer.getMainGroup().toString());
            query.setString("subGroup", offer.getSubGroup().toString());
            query.setString("type", offer.getType());
            query.setDouble("price", offer.getPrice());
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("PerformCarMatch not successfull, offerId: " + offer.getOfferId(), e);
        }
    }
    
    private List<SearchRequest> performClothesMatch(ClothesOffer offer) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from ClothesSearchRequest as u where "
                    + "u.mainGroup = :mainGroup "
                    + "and u.subGroup = :subGroup "
                    + "and u.clothesSize = :clothesSize "
                    + "and u.priceFrom <= :price "
                    + "and u.priceTo >= :price" );
            
            query.setString("mainGroup", offer.getMainGroup().toString());
            query.setString("subGroup", offer.getSubGroup().toString());
            query.setString("clothesSize", offer.getClothesSize().toString());
            query.setDouble("price", offer.getPrice());
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("PerformClothesMatch not successfull, offerId: " + offer.getOfferId(), e);
        }
    }
    
    private List<Offer> performCarMatch(CarSearchRequest request) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from CarOffer as u where "
                    + "u.mainGroup = :mainGroup "
                    + "and u.subGroup = :subGroup "
                    + "and u.type = :type "
                    + "and u.price >= :priceFrom "
                    + "and u.price <= :priceTo" );
            
            query.setString("mainGroup", request.getMainGroup().toString());
            query.setString("subGroup", request.getSubGroup().toString());
            query.setString("type", request.getType());
            query.setDouble("priceFrom", request.getPriceFrom());
            query.setDouble("priceTo", request.getPriceTo());
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("PerformCarMatch not successfull, searchId: " + request.getOfferId(), e);
        }
    }

    private List<Offer> performClothesMatch(ClothesSearchRequest request) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from ClothesOffer as u where "
                    + "u.mainGroup = :mainGroup "
                    + "and u.subGroup = :subGroup "
                    + "and u.clothesSize = :clothesSize "
                    + "and u.price >= :priceFrom "
                    + "and u.price <= :priceTo" );
            
            query.setString("mainGroup", request.getMainGroup().toString());
            query.setString("subGroup", request.getSubGroup().toString());
            query.setString("clothesSize", request.getClothesSize().toString());
            query.setDouble("priceFrom", request.getPriceFrom());
            query.setDouble("priceTo", request.getPriceTo());
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("PerformCarMatch not successfull, searchId: " + request.getOfferId(), e);
        }
    }
}
