/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.ArrayList;
import java.util.List;
import nl.krocket.push.schema.CarOffer;
import nl.krocket.push.schema.ClothesOffer;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.schema.MPSubgroup;
import nl.krocket.push.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class OfferDAO {
    
    private final SessionFactory sessionFactory;
    
    public OfferDAO() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }
    
    public void saveOffer(Offer offer) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate( offer );
            session.getTransaction().commit();
            session.close();
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Offer not saved or updated", e);
        }
    }
    
    public CarOffer getCarOffer(String offerId) {
        return (CarOffer)this.getOffer(offerId, "CarOffer");
    }
    
    public ClothesOffer getClothesOffer(String offerId) {
        return (ClothesOffer)this.getOffer(offerId, "ClothesOffer");
    }
    
    private Offer getOffer(String offerId, String tableName) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from "+tableName+" as u where u.offerId = :offerId" );
            query.setString("offerId", offerId);
            List result = query.list();
            if (!result.isEmpty()) {
                Offer m = (Offer)result.get(0);
                session.close();
                return m;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Offer not loaded, offerId:" + offerId, e);
        }
    }

    public List<Offer> getOffers(MPGroup group) {
        Session session = sessionFactory.openSession();
        String table = group.equals(MPGroup.Autos) ? "CarOffer" : "ClothesOffer";
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from "+table );
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Offers not loaded for group:" + group.name(), e);
        }
    }

    public List<Offer> getOffers(MPSubgroup supgroup) {
        Session session = sessionFactory.openSession();
        String table = supgroup.getParent().equals(MPGroup.Autos) ? "CarOffer" : "ClothesOffer";
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from "+table+" as t where t.subGroup = :subGroup" );
            query.setString("subGroup", supgroup.toString());
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Offers not loaded for subgroup:" + supgroup.name(), e);
        }
    }
    
    public List<Offer> getOffers(String deviceId) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            List result = session.createCriteria(Offer.class).add(Restrictions.eq("device.deviceId", deviceId)).list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return new ArrayList<>();
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Offers not loaded for device:" + deviceId, e);
        }
    }

    public void deleteCarOffer(String offerId) {
        this.deleteOffer(offerId, "CarOffer");
    }
    
    public void deleteClothesOffer(String offerId) {
        this.deleteOffer(offerId, "ClothesOffer");
    }
    
    private void deleteOffer(String offerId, String tableName) {
        Offer offer = getOffer(offerId, tableName);
        //clear images
        offer.getImages().clear();
        deleteOffer(offer);
    }
    
    private void deleteOffer(Offer offer) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(offer);
            session.getTransaction().commit();
            session.close();
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Offer not deleted", e);
        }
    }

}
