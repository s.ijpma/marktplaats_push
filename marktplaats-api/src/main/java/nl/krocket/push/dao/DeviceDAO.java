/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 *//*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.dao;

import java.util.List;
import nl.krocket.push.schema.MobileDevice;
import nl.krocket.push.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class DeviceDAO {
    
    private final SessionFactory sessionFactory;
    
    public DeviceDAO() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public void saveDevice(MobileDevice device) throws HibernateException {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate( device );
            session.getTransaction().commit();
            session.close();
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Device not saved or updated, deviceId: " + device.getDeviceId(), e);
        }
    }
    
    public void deleteDevice(String deviceId) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query q = session.createQuery("delete from MobileDevice as md where md.deviceId = :deviceId ");
            q.setString("deviceId", deviceId);
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Device not deleted, deviceId: " + deviceId, e);
        }
    }
    
    public List<MobileDevice> getDevices() {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery( "from MobileDevice" );
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return result;
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Devices not loaded", e);
        }
    }
    
    public MobileDevice getDevice(String deviceId) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery("from MobileDevice as md where md.deviceId = :deviceId ");
            query.setString("deviceId", deviceId);
            List result = query.list();
            if (!result.isEmpty()) {
                session.close();
                return (MobileDevice)result.get(0);
            }
            else {
                return null;
            }
        }
        catch (HibernateException e) {
            session.close();
            throw new HibernateException("Device not found, deviceId: " + deviceId, e);
        }
    }

}
