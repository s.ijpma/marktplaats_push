package nl.krocket.push.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Sander IJpma | krocket
 */
@ControllerAdvice
public class FormExceptionController {
    
    private final static Logger logger =  Logger.getLogger(FormExceptionController.class.getName());
    /**
     * Error handling method; handles binding errors 
     * which came from the @RequestBody values
     * Returns a ModelMap containing error messages
     * @param error
     * @return 
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ModelMap handleMethodArgumentNotValidException(
        MethodArgumentNotValidException error) {
        List<FieldError> errors = error.getBindingResult().getFieldErrors();
        ModelMap map = new ModelMap();
        ModelMap errorMap = new ModelMap();
        map.addAttribute("hasErrors", true);
        for (FieldError fieldError : errors) {
            errorMap.addAttribute(fieldError.getField(), fieldError.getDefaultMessage());
        }
        map.addAttribute("bindingErrors", errorMap);
        return map;
    }
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ModelMap handleOtherExceptions(Exception error, HttpServletRequest request) {
        logError(error, request);
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", true);
        ModelMap errorMap = new ModelMap();
        errorMap.addAttribute("Error Message(s):", error.getMessage());
        map.addAttribute("otherErrors", errorMap);
        return map;
    }
    
    private void logError(Exception error, HttpServletRequest request) {
        logger.error("Error " + error.getMessage(), error);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("QueryString: ").append(request.getQueryString());
        stringBuilder.append("RemoteAddr: ").append(request.getRemoteAddr());
        stringBuilder.append("RemoteHost: ").append(request.getRemoteHost());
        stringBuilder.append("RemotePort: ").append(request.getRemotePort());
        stringBuilder.append("RemoteUser: ").append(request.getRemoteUser());
        logger.error("Extra info: " + stringBuilder.toString());
    }

}
