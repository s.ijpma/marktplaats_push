package nl.krocket.push.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import nl.krocket.push.util.ReflectionHelper;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 *
 * @author Sander
 */
@Controller
@RequestMapping("/api")
public class ApiListController {

    @Autowired
    RequestMappingHandlerMapping handlerMapping;
    
    private static final Logger logger = Logger.getLogger(ApiListController.class.getName());

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView showApiList() {
        return new ModelAndView("list", "apiMethods", this.getListOfApiMethods());
    }
    
    private List<ApiDocs> getListOfApiMethods() {
        Map<RequestMappingInfo, HandlerMethod> result = this.handlerMapping.getHandlerMethods();
        List<ApiDocs> list = new ArrayList<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : result.entrySet()) {

            RequestMappingInfo requestMappingInfo = entry.getKey();
            ApiDocs docs = new ApiDocs();
            String name = requestMappingInfo.getPatternsCondition().toString();
            docs.setName(name);
            String url = name.substring(2, name.length()-1);
            docs.setUrl(url);
            String method = requestMappingInfo.getMethodsCondition().toString();
            docs.setMethod(method);
            if (method.equalsIgnoreCase("[POST]")) {
                String cls = requestMappingInfo.getParamsCondition().toString();
                if (cls!=null || !cls.isEmpty() || cls!="" ) {
                    try {
                        Object o = ReflectionHelper.getObject(cls);
                        ObjectMapper mapper = new ObjectMapper();
                        String param;
                        param = mapper.writeValueAsString(o);
                        logger.info("JSON: " + param);
                        docs.setParam(param);
                    } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        logger.error("Error serializing paramater for method: " + method, ex);
                    }
                }
            }
            
            list.add(docs);
        }
        return list;
    }
    
    
}

