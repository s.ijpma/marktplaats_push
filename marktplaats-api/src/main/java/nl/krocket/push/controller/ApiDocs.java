/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 17, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.controller;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class ApiDocs {
    
    private String name;
    private String url;
    private String method;
    private String param;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
    
    
    
}
