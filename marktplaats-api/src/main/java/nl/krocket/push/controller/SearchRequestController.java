/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.controller;

import java.util.List;
import javax.validation.Valid;
import nl.krocket.push.dao.SearchRequestDAO;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesSearchRequest;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.MPSubgroup;
import nl.krocket.push.schema.MobileDevice;
import nl.krocket.push.schema.SearchRequest;
import nl.krocket.push.services.MatchResultService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Controller
@RequestMapping("/search")
public class SearchRequestController {
    
    @RequestMapping(value = "/save/car", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap saveCarSearchRequest(@Valid @RequestBody
                            CarSearchRequest searchRequest) {
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        SearchRequestDAO service = new SearchRequestDAO();
        service.saveSearchRequest(searchRequest);
        //match and push!!
        MatchResultService matchService = new MatchResultService();
        matchService.doMatchingAndPush(searchRequest);
        return map;
    }
    
    @RequestMapping(value = "/save/clothes", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap saveClothesSearchRequest(@Valid @RequestBody
                            ClothesSearchRequest searchRequest) {
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        SearchRequestDAO service = new SearchRequestDAO();
        service.saveSearchRequest(searchRequest);
        //match and push!!
        MatchResultService matchService = new MatchResultService();
        matchService.doMatchingAndPush(searchRequest);
        return map;
    }
    
    @RequestMapping(value = "/get/car/{searchId}", method = RequestMethod.GET)
    public @ResponseBody SearchRequest getCarSearchRequest(@PathVariable String searchId) {
        SearchRequestDAO service = new SearchRequestDAO();
        return service.getCarSearchRequest(searchId);
    }
    
    @RequestMapping(value = "/get/clothes/{searchId}", method = RequestMethod.GET)
    public @ResponseBody SearchRequest getClothesOffer(@PathVariable String searchId) {
        SearchRequestDAO service = new SearchRequestDAO();
        return service.getClothesSearchRequest(searchId);
    }
    
    @RequestMapping(value = "/list/device/{deviceId}", method = RequestMethod.GET)
    public @ResponseBody List<SearchRequest> getSearchRequestsForDevice(@PathVariable String deviceId) {
        SearchRequestDAO service = new SearchRequestDAO();
        return service.getSearchRequests(deviceId);
    }
    
    @RequestMapping(value = "/delete/car/{searchId}", method = RequestMethod.DELETE)
    public @ResponseBody ModelMap deleteCarSearchRequest(@PathVariable String searchId) {
        SearchRequestDAO service = new SearchRequestDAO();
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        service.deleteCarSearchRequest(searchId);
        return map;
    }
    
    @RequestMapping(value = "/delete/clothes/{searchId}", method = RequestMethod.DELETE)
    public @ResponseBody ModelMap deleteClothesSearchRequest(@PathVariable String searchId) {
        SearchRequestDAO service = new SearchRequestDAO();
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        service.deleteClothesSearchRequest(searchId);
        return map;
    }

}
