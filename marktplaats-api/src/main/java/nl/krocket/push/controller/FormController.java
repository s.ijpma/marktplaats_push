/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 17, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Controller
public class FormController {

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public ModelAndView fileUploadForm() {
       return new ModelAndView("upload");
    }
   
}
