/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 14, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.controller;

import javax.servlet.http.HttpServletRequest;
import nl.krocket.push.config.Initializer;
import nl.krocket.push.schema.OfferImage;
import nl.krocket.push.util.FileHelper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Controller
@RequestMapping("/offer")
public class ImageUploadController {
    
    static final Logger logger = Logger.getLogger(ImageUploadController.class.getName());
    
    @RequestMapping (value = "/image/upload", method = RequestMethod.POST)  
    public @ResponseBody OfferImage uploadFile(@RequestParam(value = "imageFile") MultipartFile multipartFile, HttpServletRequest request) {
        String fileName;
        FileHelper helper = new FileHelper();
        fileName = helper.saveFile(multipartFile);
        
        OfferImage image = new OfferImage();
        image.setName(fileName);
        String baseUrl = String.format("%s://%s:%d/",request.getScheme(),  request.getServerName(), request.getServerPort());
        image.setUrl(baseUrl + "marktplaats-api/uploads/" +  fileName);
        return image;
    }

}
