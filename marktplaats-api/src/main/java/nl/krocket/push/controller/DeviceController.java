package nl.krocket.push.controller;

import java.util.List;
import javax.validation.Valid;
import nl.krocket.push.schema.MobileDevice;
import nl.krocket.push.dao.DeviceDAO;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Sander IJpma | krocket
 */
@Controller
@RequestMapping("/device")
public class DeviceController {
    
    Logger logger = Logger.getLogger(DeviceController.class.getName());
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap saveDevice(@Valid @RequestBody MobileDevice device) {
        ModelMap map = new ModelMap();
        DeviceDAO service = new DeviceDAO();
        try {
            service.saveDevice(device);
            map.addAttribute("hasErrors", false);
        }
        catch (HibernateException e) {
            map.addAttribute("hasErrors", true);
            ModelMap errorMap = new ModelMap();
            errorMap.addAttribute("Error Message(s):", e.getMessage());
            map.addAttribute("otherErrors", errorMap);
        }
        return map;
    }
    
    @RequestMapping(value = "/delete/{deviceId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ModelMap deleteDevice(@PathVariable String deviceId) {
        ModelMap map = new ModelMap();
        DeviceDAO service = new DeviceDAO();
        try {
            service.deleteDevice(deviceId);
            map.addAttribute("hasErrors", false);
        }
        catch (HibernateException e) {
            map.addAttribute("hasErrors", true);
            ModelMap errorMap = new ModelMap();
            errorMap.addAttribute("Error Message(s):", e.getMessage());
            map.addAttribute("otherErrors", errorMap);
        }
        return map;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<MobileDevice> getDevices() {
        DeviceDAO service = new DeviceDAO();
        return service.getDevices();
    }
    

}
