package nl.krocket.push.controller;

import java.util.List;
import nl.krocket.push.schema.ClothesSize;
import nl.krocket.push.schema.MPGroup;
import nl.krocket.push.schema.MPSubgroup;
import nl.krocket.push.schema.MobileDeviceType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Sander IJpma | krocket
 */
@Controller
@RequestMapping("/data")
public class FormDataController {
    
    @RequestMapping(value = "/group/list", method = RequestMethod.GET)
    public @ResponseBody List<MPGroup> getCatagoryList() {
        return MPGroup.getAsList();
    }
    
    @RequestMapping(value = "/subgroup/list/{group}", method = RequestMethod.GET)
    public @ResponseBody List<MPSubgroup> getCatagorySubList(@PathVariable MPGroup group) {
        return MPSubgroup.getAsList(group);
    }
    
    @RequestMapping(value = "/clothes/size/list", method = RequestMethod.GET)
    public @ResponseBody List<ClothesSize> getClothesSizeList() {
        return ClothesSize.getAsList();
    }
    
    @RequestMapping(value = "/device/type/list", method = RequestMethod.GET)
    public @ResponseBody List<MobileDeviceType> getDeviceTypeList() {
        return MobileDeviceType.getAsList();
    }
    
    
}
