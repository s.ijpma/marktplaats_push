/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 13, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import nl.krocket.push.schema.Offer;
import nl.krocket.push.dao.OfferDAO;
import nl.krocket.push.schema.CarOffer;
import nl.krocket.push.schema.CarSearchRequest;
import nl.krocket.push.schema.ClothesOffer;
import nl.krocket.push.schema.ClothesSearchRequest;
import nl.krocket.push.services.MatchResultService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
@Controller
@RequestMapping("/offer")
public class OfferController {
    
    @RequestMapping(value = "/save/car", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap saveCarOffer(@Valid @RequestBody
                            CarOffer offer) {
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        OfferDAO service = new OfferDAO();
        service.saveOffer(offer);
        //match and push!!
        MatchResultService matchService = new MatchResultService();
        matchService.doMatchingAndPush(offer);
        return map;
    }
    
    @RequestMapping(value = "/save/clothes", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap saveClothesOffer(@Valid @RequestBody
                            ClothesOffer offer) {
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        OfferDAO service = new OfferDAO();
        service.saveOffer(offer);
        //match and push!!
        MatchResultService matchService = new MatchResultService();
        matchService.doMatchingAndPush(offer);
        return map;
    }
    
    @RequestMapping(value = "/get/car/{offerId}", method = RequestMethod.GET)
    public @ResponseBody Offer getCarOffer(@PathVariable String offerId) {
        OfferDAO service = new OfferDAO();
        return service.getCarOffer(offerId);
    }
    
    @RequestMapping(value = "/get/clothes/{offerId}", method = RequestMethod.GET)
    public @ResponseBody Offer getClothesOffer(@PathVariable String offerId) {
        OfferDAO service = new OfferDAO();
        return service.getClothesOffer(offerId);
    }
    
    @RequestMapping(value = "/list/device/{deviceId}", method = RequestMethod.GET)
    public @ResponseBody List<Offer> getOffersForDevice(@PathVariable
                            String deviceId) {
        OfferDAO service = new OfferDAO();
        List<Offer> offerList = service.getOffers(deviceId);
        List<Offer> newList = new ArrayList<>();
        for (Offer offer : offerList) {
            if (offer instanceof CarSearchRequest) {
                //do nothing!
            }
            else if (offer instanceof ClothesSearchRequest) {
                //do nothing!
            }
            else {
                newList.add(offer);
            }
        }
        return newList;
    }
    
    @RequestMapping(value = "/delete/car/{offerId}", method = RequestMethod.DELETE)
    public @ResponseBody ModelMap deleteCarOffer(@PathVariable String offerId) {
        OfferDAO service = new OfferDAO();
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        service.deleteCarOffer(offerId);
        return map;
    }
    
    @RequestMapping(value = "/delete/clothes/{offerId}", method = RequestMethod.DELETE)
    public @ResponseBody ModelMap deleteClothesOffer(@PathVariable String offerId) {
        OfferDAO service = new OfferDAO();
        ModelMap map = new ModelMap();
        map.addAttribute("hasErrors", false);
        service.deleteClothesOffer(offerId);
        return map;
    }

}
