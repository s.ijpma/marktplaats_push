package nl.krocket.push.util;

import java.util.Date;

/**
 *
 * @author Sander IJpma | krocket
 */
public class DateUtil {
    
    public static boolean equals(Date date1, Date date2) {
        long timeStamp1 = date1.getTime();
        long timeStamp2 = date2.getTime();
        return timeStamp1 == timeStamp2;
    }
}
