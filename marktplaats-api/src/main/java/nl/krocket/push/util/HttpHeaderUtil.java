package nl.krocket.push.util;

import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.codec.Base64;

/**
 *
 * @author Sander IJpma | krocket
 */
public class HttpHeaderUtil {
    
        public static HttpHeaders getJsonHttpHeadersBasicAuth(String userName, String passWord) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json;charset=UTF-8");
            String combinedUsernamePassword = userName+":"+passWord;
            byte[] base64Token = Base64.encode(combinedUsernamePassword.getBytes());
            String base64EncodedToken = new String (base64Token);
            //adding Authorization header for HTTP Basic authentication
            headers.add("Authorization","Basic  "+base64EncodedToken);
            return headers;
        }
}
