/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 16, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.util;

import com.urbanairship.api.push.model.notification.richpush.RichPushMessage;
import java.util.Map;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class RichPushMessageHelper {
    
    private static final VelocityEngine VELOCITY_ENGINE = new VelocityEngine();
    private static final String ENCODING = "UTF-8";
    
    
    
    public static String getBody(String template, Map model) {
        //VELOCITY_ENGINE.addProperty("file.resource.loader.path", "c:\\temp\\");
        VELOCITY_ENGINE.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        VELOCITY_ENGINE.setProperty("classpath.resource.loader.class",ClasspathResourceLoader.class.getName());
        VELOCITY_ENGINE.setProperty("input.encoding", ENCODING);
        VELOCITY_ENGINE.setProperty("output.encoding", ENCODING);
        String text = VelocityEngineUtils.mergeTemplateIntoString(
                        VELOCITY_ENGINE, template, model);
        return text;
    }
    
    
    public static RichPushMessage getHtmlMessage( String title, String body ) {
        return RichPushMessage.newBuilder()
                .setTitle(title)
                .setContentType("text/html")
                .setBody(body)
                .build();
    }

}
