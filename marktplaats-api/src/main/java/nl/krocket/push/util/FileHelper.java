package nl.krocket.push.util;

import java.io.File;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Sander IJpma | krocket
 */
public class FileHelper {
    
    static final Logger logger = Logger.getLogger(FileHelper.class.getName());
    
    public String saveFile(MultipartFile file) {
        String tempFileName = file.getOriginalFilename();

        int idx = tempFileName.lastIndexOf("\\");
        if (idx != -1) {
            tempFileName = tempFileName.substring(idx);
        }
        String succeed = "";
        try {
            file.transferTo(new File(tempFileName));
            succeed = "" + tempFileName;
        } catch (IOException ex) {
            logger.error("IO error saving file", ex);
        } catch (IllegalStateException ex) {
            logger.error("Error saving file", ex);
        }
        return succeed;
    }
    
}
