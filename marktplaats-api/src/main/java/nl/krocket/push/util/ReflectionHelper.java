package nl.krocket.push.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import nl.krocket.push.schema.CarOffer;

public class ReflectionHelper {
    
    public static Object getObject(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Class c = Class.forName(className.replace("]", "").replace("[", ""), false, CarOffer.class.getClassLoader());
        Object o=null;
        if (c!=null) {
            o = c.newInstance();
            if (!c.isEnum()) {
                o = ReflectionHelper.runSetMethods(o);
            }
        }
        return o;
    }

    /**
     * Method wich returns the classes contained in given packagename
     *
     * @param packageName
     * @return {@link Class}<?>[]
     * @throws ClassNotFoundException
     */
    public static Class<?>[] getClassesInPackage(String packageName) throws ClassNotFoundException {
        Class<?>[] result;
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL packageURL = loader.getResource(
                packageName.replace('.', '/'));
        File directory = new File(packageURL.getFile());
        String[] dirList = directory.list();
        result = new Class<?>[dirList.length];
        for (int i = 0; i < dirList.length; i++) {
            File classFile = new File(packageURL.getFile() + File.separator + dirList[i]);
            if (classFile.getAbsoluteFile().getName().endsWith(".class") & (!classFile.getAbsoluteFile().getName().endsWith("Test.class"))) {
                Class<?> packageClass = Class.forName(packageName + '.' + classFile.getName().substring(0, classFile.getName().length() - 6));
                result[i] = packageClass;
            }
        }
        return result;
    }

    /**
     * Method wich returns the classes contained in given packagename
     *
     * @param packageName
     * @return {@link Class}<?>[]
     * @throws ClassNotFoundException
     */
    public static Class<?>[] getTestClassesInPackage(String packageName) throws ClassNotFoundException {
        Class<?>[] result;
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL packageURL = loader.getResource(
                packageName.replace('.', '/'));
        File directory = new File(packageURL.getFile());
        String[] dirList = directory.list();
        result = new Class<?>[dirList.length];
        for (int i = 0; i < dirList.length; i++) {
            File classFile = new File(packageURL.getFile() + File.separator + dirList[i]);
            if ((classFile.getAbsoluteFile().getName().endsWith("Test.class"))) {
                Class<?> packageClass = Class.forName(packageName + '.' + classFile.getName().substring(0, classFile.getName().length() - 6));
                result[i] = packageClass;
            }
        }
        return result;
    }

    /**
     * Method wich runs all set methods on given object and given parameter
     * values. Note that sorting of these params is in alfabetical order.
     *
     * @param onObject
     * @param params
     * @return
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     */
    public static Object runSetMethods(Object onObject, Object[] params) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Object result = onObject;
        Method[] methods = onObject.getClass().getMethods();

        //sort methods
        methods = sortMethods(methods);

        int i = 0;
        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                result = runMethod(method, onObject, params[i]);
                i++;
            }
        }
        return result;
    }

    /**
     * Method wich runs all set methods on given object.
     *
     * @param onObject
     * @return
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.InstantiationException
     * @throws java.lang.reflect.InvocationTargetException
     */
    public static Object runSetMethods(Object onObject) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
        Object result = onObject;
        Method[] methods = onObject.getClass().getMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                result = runMethod(method, onObject);
            }
        }
        return result;
    }

    /**
     * Method which runs a given method on a given object.
     *
     * @param methodToBeRun
     * @param onObject
     * @param paramValue
     * @return
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     */
    public static Object runMethod(Method methodToBeRun, Object onObject, Object paramValue) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        methodToBeRun.invoke(onObject, paramValue);
        return onObject;
    }

    /**
     * Method which runs a given method on a given object.
     *
     * @param methodToBeRun
     * @param onObject
     * @return
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws java.lang.InstantiationException
     */
    public static Object runMethod(Method methodToBeRun, Object onObject) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
        Class<?>[] params = methodToBeRun.getParameterTypes();
        Object[] args = new Object[params.length];
        for (int i = 0; i < params.length; i++) {
            if (!params[i].isArray()) {
                if (params[i].getName().equalsIgnoreCase(Integer.class.getName()) || params[i].getName().equalsIgnoreCase(int.class.getName())) {
                    Random r = new Random();
                    args[i] = r.nextInt();
                } else if (params[i].getName().equalsIgnoreCase(Long.class.getName()) || params[i].getName().equalsIgnoreCase(long.class.getName())) {
                    Random r = new Random();
                    args[i] = r.nextLong();
                } else if (params[i].getName().equalsIgnoreCase(Double.class.getName()) || params[i].getName().equalsIgnoreCase(double.class.getName())) {
                    Random r = new Random();
                    args[i] = r.nextDouble();                    
                } else if (params[i].getName().equalsIgnoreCase(Boolean.class.getName()) || params[i].getName().equalsIgnoreCase(boolean.class.getName())) {
                    Random r = new Random();
                    args[i] = r.nextBoolean();
                } else if (params[i].getName().equalsIgnoreCase(String.class.getName())) {
                    args[i] = UUID.randomUUID().toString();
                } else if (params[i].getName().equalsIgnoreCase(Date.class.getName())) {
                    args[i] = new Date();
                } else {
                    Object actual = getInstance(params[i]);
                    args[i] = runSetMethods(actual);
                }
            } else {
                Class<?> arrClass = params[i].getComponentType();
                Object actual = Array.newInstance(arrClass, 2);
                Object actual1 = getInstance(arrClass);
                actual1 = runSetMethods(actual1);
                Object actual2 = getInstance(arrClass);
                actual2 = runSetMethods(actual2);
                Array.set(actual, 0, actual1);
                Array.set(actual, 1, actual2);
                args[i] = actual;
            }
        }
        methodToBeRun.invoke(onObject, args);
        return onObject;
    }

    /**
     * Gets instance of given class. All properties of instance are null.
     *
     * @param classToBeInstantiated
     * @return
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    public static Object getInstance(Class<?> classToBeInstantiated) throws InstantiationException, IllegalAccessException {
        Object result;
        if (!classToBeInstantiated.isEnum()) {
            result = classToBeInstantiated.newInstance();
        }
        else {
            result = classToBeInstantiated.getEnumConstants()[0];
        }
        return result;
    }

    /**
     * Returns a copy of the object, or null if the object cannot be serialized.
     *
     * @param orig
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object copy(Object orig) throws IOException, ClassNotFoundException {
        Object obj;
        // Write the object out to a byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (ObjectOutputStream out = new ObjectOutputStream(bos)) {
            out.writeObject(orig);
            out.flush();
        }

        // Make an input stream from the byte array and read
        // a copy of the object back in.
        ObjectInputStream in = new ObjectInputStream(
                new ByteArrayInputStream(bos.toByteArray()));
        obj = in.readObject();

        return obj;
    }

    @SuppressWarnings("unchecked")
    public static void runMethodsOfClass(Class t, Object[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassLoader cl = t.getClassLoader();
        Class classToBeRun = cl.loadClass(t.getName());
        Object o = classToBeRun.newInstance();

        Method[] methods = classToBeRun.getMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("test")) {
                method.invoke(o, args);
            }
        }
    }
    
    private static Method[] sortMethods(Method[] methods) {

        List<Method> methodList = new ArrayList<>();

        methodList.addAll(Arrays.asList(methods));
        
        Collections.sort(methodList, new Comparator<Method>(){

            @Override
            public int compare(Method method1, Method method2) {
                int result = 0;
                if (method1 != null && method2 != null) {
                    result = method1.getName().compareTo(method2.getName());
                }
                return result;
                }
 
        });
        Method[] result = new Method[methodList.size()-1];
        return methodList.toArray(result);
    }

}



