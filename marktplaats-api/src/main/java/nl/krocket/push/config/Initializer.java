package nl.krocket.push.config;

import java.io.File;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 * @author Sander IJpma | Krocket
 */
public class Initializer implements WebApplicationInitializer {
    
    public static final String UPLOAD_DIR = "uploads";

    @Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {
        AnnotationConfigWebApplicationContext mvcContext = new AnnotationConfigWebApplicationContext();
        mvcContext.register(MvcConfig.class);
        servletContext.addListener(new ContextLoaderListener(mvcContext));
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
                "spring", new DispatcherServlet(mvcContext));
        dispatcher.setLoadOnStartup(1);

        // gets absolute path of the web application
        String applicationPath = servletContext.getRealPath("");
        // constructs path of the directory to save uploaded file
        String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;

        dispatcher.setMultipartConfig(new MultipartConfigElement(uploadFilePath));
        dispatcher.addMapping("/marktplaats-api/*");
    }
}
