/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 14, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.config;

import java.lang.reflect.Method;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class ExtendedRequestMappingHandlerMapping extends RequestMappingHandlerMapping {
    @Override
    protected RequestCondition<?> getCustomTypeCondition(Class<?> handlerType) {
            RoleMapping typeAnnotation = AnnotationUtils.findAnnotation(handlerType, RoleMapping.class);
            return createCondition(typeAnnotation);
    }

    @Override
    protected RequestCondition<?> getCustomMethodCondition(Method method) {
            RoleMapping methodAnnotation = AnnotationUtils.findAnnotation(method, RoleMapping.class);
            return createCondition(methodAnnotation);
    }

    private RequestCondition<?> createCondition(RoleMapping accessMapping) {
            return (accessMapping != null) ? new RolesRequestCondition(accessMapping.value()) : null;
    }
}
