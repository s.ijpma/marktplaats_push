/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 14, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.config;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.mvc.condition.RequestCondition;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class RolesRequestCondition implements RequestCondition<RolesRequestCondition> {

	private final Set<String> roles;

	public RolesRequestCondition(String... roles) {
		this(Arrays.asList(roles));
	}

	private RolesRequestCondition(Collection<String> roles) {
		this.roles = Collections.unmodifiableSet(new HashSet<String>(roles));
	}

	@Override
	public RolesRequestCondition combine(RolesRequestCondition other) {
		Set<String> allRoles = new LinkedHashSet<String>(this.roles);
		allRoles.addAll(other.roles);
		return new RolesRequestCondition(allRoles);
	}

	@Override
	public RolesRequestCondition getMatchingCondition(HttpServletRequest request) {
		for (String role : this.roles) {
			if (!request.isUserInRole(role)) {
				return null;
			}
		}
		return this;
	}

	@Override
	public int compareTo(RolesRequestCondition other, HttpServletRequest request) {
		return other.roles.size() - this.roles.size();
	}

}
