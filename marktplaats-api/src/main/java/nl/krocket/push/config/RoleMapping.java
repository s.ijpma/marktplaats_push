/*
 * Copyright(c) 2014 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on Mar 14, 2014 by Sander using UTF-8 encoding
 */

package nl.krocket.push.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
/**
 * Defines the roles required to map a controller method to a request.
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RoleMapping {

	/**
	 * A list of required security roles (e.g. "ROLE_ADMIN").
	 */
	String[] value() default {};

}
