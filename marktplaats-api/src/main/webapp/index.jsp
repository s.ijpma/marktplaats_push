<!DOCTYPE html>
<html>
    <head>
        <title>Marktplaats API Demo Server</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="css/bootstrap/v2.3.2/theme/normal/bootstrap.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="css/bootstrap/v2.3.2/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen"/>

        <link href="css/fuelux/v2.3.2/fuelux.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="css/fuelux/v2.3.2/fuelux-responsive.min.css" type="text/css" rel="stylesheet" media="screen"/>

        <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript" src="js/fuelux/v2.3.2/loader.js"></script>
        
        <script type="text/javascript" src="js/jquery.json-2.2.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="header">
            </div>
            <div class="fuelux">
                <a href="/marktplaats-api/marktplaats-api/api/list"> api docs </a>
                <a href="/marktplaats-api/marktplaats-api/upload"> upload </a>
            </div>
        </div>
        <script type="text/javascript">

            

            $(document).ready(function() {
                
                
            });
        </script>
    </body>
</html>