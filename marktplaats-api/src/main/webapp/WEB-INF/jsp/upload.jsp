<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="../../css/bootstrap/v2.3.2/theme/normal/bootstrap.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="../../css/bootstrap/v2.3.2/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen"/>

        <link href="../../css/fuelux/v2.3.2/fuelux.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="../../css/fuelux/v2.3.2/fuelux-responsive.min.css" type="text/css" rel="stylesheet" media="screen"/>

        <script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
        
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript" src="../../js/fuelux/v2.3.2/loader.js"></script>
        
        <script type="text/javascript" src="../../js/jquery.json-2.2.min.js"></script>
    </head>
    <body>
        <div class="container">
        <form id="uploadForm" action="/marktplaats-api/marktplaats-api/offer/image/upload" enctype="multipart/form-data" method="POST">
            <div class="row">
                <label class="label">Bestand</label> 
                <input type="file" name="imageFile" accept="image/*"/>
            </div>
            <div class="row">
                <input id="submitBtn" name="Submit" type="submit"/>
            </div>
        </form>
        </div>
    </body>
</html>
