<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<title>Marktplaats API Endpoints</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="../../css/bootstrap/v2.3.2/theme/normal/bootstrap.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="../../css/bootstrap/v2.3.2/bootstrap-responsive.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="../../css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen"/>

        <link href="../../css/fuelux/v2.3.2/fuelux.min.css" type="text/css" rel="stylesheet" media="screen"/>
        <link href="../../css/fuelux/v2.3.2/fuelux-responsive.min.css" type="text/css" rel="stylesheet" media="screen"/>

        <script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
        
        <script type="text/javascript" src="../../js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript" src="../../js/fuelux/v2.3.2/loader.js"></script>
        
        <script type="text/javascript" src="../../js/jquery.json-2.2.min.js"></script>
        
<link rel="stylesheet" href="http://yandex.st/highlightjs/8.0/styles/default.min.css">
<script src="http://yandex.st/highlightjs/8.0/highlight.min.js"></script>

<script>
    hljs.configure({tabReplace: '    '});
    hljs.initHighlightingOnLoad();
</script>
  
</head>
<body>
  <div class="container">
    <h1>Marktplaats API Endpoints</h1>
    <c:forEach items="${apiMethods}" var="entry">
        <p>
            <a href="/marktplaats-api/marktplaats-api/${entry.getUrl()}">${entry.getName()}</a> ${entry.getMethod()} 
            <c:if test="${entry.getParam()!=null}">
                <pre><code>${entry.getParam()}</code></pre>
            </c:if>
        </p>
    </c:forEach>
  </div>
 
</body>
</html>